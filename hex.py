num = input("Hex number: ")

def hex(decimal):
    places = []
    letters = {"a": 10, "b": 11, "c": 12, "d": 13, "e": 14, "f": 15}
    for char in decimal:
        places.append(char)
    i = 0
    j = len(places) - 1
    while i < len(places):
        if "a" <= places[i] <= "f":
            places[i] = letters[places[i]]
        elif "g" <= places[i] <= "z":
            raise ValueError("not a valid value for hexidecimal")
        places[i] = int(places[i]) * (16 ** j)
        i += 1
        j -= 1
    return sum(places)


print(hex(num))
